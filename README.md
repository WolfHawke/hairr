# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Creating truly random passphrases, passwords or numbers is nearly impossible in a computer, because computers cannot generate truly random numbers. And so computer-generated random numbers and strings can easily be cracked, especially if the attacker knows the algorithm for generating said string. Thankfully Arnold Reinhold created the Diceware system at [http://world.std.com/~reinhold/diceware.html](http://world.std.com/~reinhold/diceware.html), which uses dice to create truly random passphrases. However, none of the Diceware-based apps out there actually use dice to create passphrases, until now. Hawke AI Random Roller uses real dice to create the truly random passphrases, passwords, decimal or hexadecimal numbers. The word lists and algorithms are based on Mr. Reinhold’s work. Random Roller simply provides an easy-to-use front end so that we’re not wasting precious paper and so you can generate your passphrases more quickly.

Hawke AI Random Roller is completely self-contained and will run in most any web browser. As it is completely JavaScript-based, there is no talking back to any servers, making it possible to completely sandbox your items

* Version: 0.3
* Used successfully in: Firefox, Chrome, Microsoft Edge, Brave

### How do I get set up? ###

* Download the repository and extract it to your hard drive.
* Open the folder you extracted the file from and open index.html in your favorite browser.

### Know issues ###

* When the program is opened in Microsoft's browswers (Internet Explorer or Edge), it does not advance from the configuration screen. The logo needs to beclicked


### Contribution guidelines ###

* Currently the program is only available in English. The aim is to internationalize it. Translations can be submitted to the repository owner.
* This is released under GNU GPL v.3.0. Feel free to download and customize.
* Please pass corrections to the code back to the repo owner.

### Who do I talk to? ###

* Repo owner or admin