/*
 * Diceware String Generator Helper
 * Javascript File
 *
 * Copyright © 2015, 2017, 2019 Hawke AI
 *
 * Last Updated: 2019-01-07 | JMW
 *
 */

/* global variables */
window.errorState = false;
window.passphrase = [];
window.passPhraseType = "";
window.defaultPrefs = {
	"show_phrase" : "true",
	"show_ascii" : "true",
	"show_alphanum" : "true",
	"show_decimal" : "true",
	"show_hex" : "false",
	"init_tab" : "phrase",
	"phrase_spacer" : "char",
	"phrase_caps" : "false"
};
window.prefs = {};
window.lang = {
	"language" : "en-us",
	"success" : "false",
	"dicewords" : "Diceware&trade; Passphrase",
	"diceasciitab" : "Random Characters",
	"dicealphanum" : "Numbers and Letters",
	"dicenum" : "Decimals",
	"dicehex" : "Hexadecimals"
};
/* languages available as comma-separated string */
window.langAvailable = "en-us";
window.version = "0.4";
window.initializing = true;

/* Diceware generation functions */
/*
 * Name: findVal
 * Description: finds a given value in the diceware list
 * Arguments: number = dice number to find value of
 * Returns: found value
 */
function findVal(number) {
	var search_array = [];
	var result = "";
	if ((number > 10) && (number < 67)) {
		search_array = chars;
	} else if ((number > 11110) && number < (16660)) {
		search_array = one;
	} else if ((number > 21110) && number < (26660)) {
		search_array = two;
	} else if ((number > 31110) && number < (36660)) {
		search_array = three;
	} else if ((number > 41110) && number < (46660)) {
		search_array = four;
	} else if ((number > 51110) && number < (56660)) {
		search_array = five;
	} else if ((number > 61110) && number < (66660)) {
		search_array = six;
	}
	for (var i = 0; i < search_array.length; i++) {
		if (search_array[i].indexOf(number) > -1) {
			result = search_array[i].substr(search_array[i].indexOf(":") + 1);
			return result;
		}
	}
}


/*
 * Name: genAsciiCode
 * Description: generates an Ascii code character depending on dice roll
 * Arguments: number = dice number to find value (must be greater than 3)
 * Returns: generated character
 */
function genAsciiCode(number) {
	if (number < 111 || number > 666) {
		return false;
	}
	var first_roll = Math.floor(number / 100);
	var second_roll = Math.floor((number - (first_roll * 100)) / 10);
	var third_roll = number - (first_roll * 100) - (second_roll * 10);
	var one_two = [["A", "B", "C", "D", "E", "F"], ["G", "H", "I", "J", "K", "L"], ["M", "N", "O", "P", "Q", "R"], ["S", "T", "U", "V", "W", "X"], ["Y", "Z", "0", "1", "2", "3"], ["4", "5", "6", "7", "8", "9"]];
	var three_four = [["a", "b", "c", "d", "e", "f"], ["g", "h", "i", "j", "k", "l"], ["m", "n", "o", "p", "q", "r"], ["s", "t", "u", "v", "w", "x"], ["y", "z", "0", "1", "2", "3"], ["4", "5", "6", "7", "8", "9"]];
	var five_six = [["~", "!", "@", "#", "$", "%"], ["^", "&amp;", "*", "(", ")", "_"], ["+", "-", "=", "[", "]", "{"], ["}", ":", ";", "'", "&quot;", ","], [".", "<", ">", "/", "?", "\\"], ["|", "&grave;", "!", "&num;", "%", "$"]];

	var first = null;
	var second = null;

	/* find first roll */
	switch (first_roll) {
	case 1:
	case 2:
		first = one_two;
		break;

	case 3:
	case 4:
		first = three_four;
		break;

	case 5:
	case 6:
		first = five_six;
		break;
	}

	/* find second roll */
	second = first[second_roll - 1];

	return second[third_roll - 1];

}

/*
 * Name: genAlphaNumeric
 * Description: generates an alphanumeric string with caps and numbers
 * Arguments: number = dice roll to generate from
 * Returns: generated chararcter
 */
function genAlphaNumeric(number) {
	if (number < 111 || number > 666) {
		return false;
	}
	var first_roll = Math.floor(number / 100);
	var second_roll = Math.floor((number - (first_roll * 100)) / 10);
	var third_roll = number - (first_roll * 100) - (second_roll * 10);

	var table = [["A", "B", "C", "D", "E", "F"], ["G", "H", "I", "J", "K", "L"], ["M", "N", "O", "P", "Q", "R"], ["S", "T", "U", "V", "W", "X"], ["Y", "Z", "0", "1", "2", "3"], ["4", "5", "6", "7", "8", "9"]];
	var first = table[first_roll - 1];
	var second = first[second_roll - 1];
	if (third_roll == 1 || third_roll == 3 || third_roll == 5) {
		if (! $.isNumeric(second)) {
			second = second.toLowerCase();
		}
	}
	return second;
}

/*
 * Name: genRandomNumber
 * Description: generates a random number from dice roll
 * Arguments: number = dice roll to evaluate
 * Returns: generated number
 */
function genRandomNumber(number) {
	if (number < 11 || number > 66) {
		if (number > 110 && number < 667) {
			number = Math.floor(number / 10);
			/* take a 3-digit roll down to 2 */
		} else {
			return false;
		}
	}
	var first_roll = Math.floor(number / 10);
	var second_roll = number - (first_roll * 10);

	if (first_roll > 5) {
		return "*";
	}
	if (second_roll == 2 || second_roll == 4 || second_roll == 6) {
		if (first_roll + 5 == 10) {
			return "0";
		} else {
			return first_roll + 5;
		}
	} else {
		return first_roll;
	}
}

/*
 * Name genHexNumber
 * Description: generates a random hexadecimal number
 * Argumetns: number = dice roll to evaluate
 * Returns: generated number
 */
function genHexNumber(number) {
	if (number < 11 || number > 66) {
		if (number > 110 && number < 667) {
			number = Math.floor(number / 10);
			/* take a 3-digit roll down to 2 */
		} else {
			return false;
		}
	}
	var first_roll = Math.floor(number / 10);
	var second_roll = number - (first_roll * 10);
	var table = [["0", "1", "2", "3", "4", "5"], ["6", "7", "8", "9", "A", "B"], ["C", "D", "E", "F", "0", "1"], ["2", "3", "4", "5", "6", "7"], ["8", "9", "A", "B", "C", "D"], ["E", "F", "D", "E", "1", "9"]];

	var second = table[first_roll - 1];
	return second[second_roll - 1];
}

/* Interface functions */

/*
 * Name: addTab
 * Description: adds a tab to the list
 * Arguments: id = identifier of the tab to add:
 * 				   "phrase","ascii","lettnum","dec","hex"
 *            selectonly = adds select only
 * 			  init = whether or not we're initializing
 * Returns: nothing
 */
function addTab(id,selectonly,init) {
	if (typeof id == "undefined") { return false; }
	if (typeof selectonly == "undefined" ) { selectonly = false; }
	if (typeof init == "undefined") { init = false; }
	var active = "";
	var display = false;
	var label = "";
	if (prefs.init_tab == id) {
		active = " selected";
		display = true;
	}
	switch (id) {
		case "phrase":
			label = lang.dicewords;
			break;
		case "ascii":
			label = lang.diceasciitab;
			break;
		case "lettnum":
			label = lang.dicealphanum;
			break;
		case "dec":
			label = lang.dicenum;
			break;
		case "hex":
			label = lang.dicehex;
			break;
	}
	$("#tablist").append("<option value=\"" + id + "\"" + active + ">" + label + "</option>");
	if (!selectonly) {
		$("div#interface").append("<div id=\"" + id + "\">" + tabContent(id) + "</div>");
	}
	if (!init) {
		$("#tablist").selectmenu("refresh");
	}
}

/*
 * Name: applyPrefs
 * Description: applies all preferences to app
 * Arguments: init = whether or not we're initializing
 * Returns: nothing
 */
function applyPrefs(init) {
	if (typeof init == "undefined") { init = false; }
	var action = "";
	var selonly = false;
	var spacers = ["none","char","space"];

	if (!init) {
		$("#tablist").html("");
	}

 	if (prefs.show_phrase == "true") {
 		if ($("#phrase").length == 0) {
			selonly = false;
		} else {
			selonly = true;
		}
		addTab("phrase",selonly,init);
	} else {
		if ($("#phrase").length) {
			$("#phrase").remove();
		}
	}
 	if (prefs.show_ascii == "true") {
 		if ($("#ascii").length == 0) {
			selonly = false;
		} else {
			selonly = true;
		}
		addTab("ascii",selonly,init);
	} else {
		if ($("#ascii").length) {
			$("#ascii").remove();
		}
	}
	 if (prefs.show_alphanum == "true") {
 		if ($("#lettnum").length == 0) {
			selonly = false;
		} else {
			selonly = true;
		}
		addTab("lettnum",selonly,init);
	} else {
		if ($("#lettnum").length) {
			$("#lettnum").remove();
		}
	}
	 if (prefs.show_decimal == "true") {
 		if ($("#dec").length == 0) {
			selonly = false;
		} else {
			selonly = true;
		}
		addTab("dec",selonly,init);
	} else {
		if ($("#dec").length) {
			$("#dec").remove();
		}
	}
	 if (prefs.show_hex == "true") {
 		if ($("#hex").length == 0) {
			selonly = false;
		} else {
			selonly = true;
		}
		addTab("hex",selonly,init);
	} else {
		if ($("#hex").length) {
			$("#hex").remove();
		}
	}

	if (!init) { $("#interface").trigger("create"); }

	/* apply phrase preferences */
	 if ($("#phrase").length) {
		$("#spacer_" + prefs.phrase_spacer).prop("checked",true);
		if (prefs.phrase_spacer == "char") {
			toggleChars("show");
		} else {
			toggleChars("hide");
		}
		if (prefs.phrase_caps == "true") {
			$("#diceword_caps").prop("checked",true);
			if ($("#addcaps").css("display") == "none") { toggleCaps(); }
		} else {
			$("#diceword_caps").prop("checked",false);
			if ($("#addcaps").css("display") != "none") { toggleCaps(); }
		}
		if (!init) {
			for (var i=0; i<spacers.length; i++){
				$("#spacer_" + spacers[i]).checkboxradio("refresh");
			}
			$("#diceword_caps").checkboxradio("refresh");
		}
	 }



	/* show selected tab */
	showTab();

}

/*
 * Name: checkForLocalStorage
 * Description: Makes sure browser supports local storage; pilfered from diveintohtml5
 * Arguments: none
 * Returns: false or localStorage item
 */
function checkForLocalStorage() {
	try {
		return 'localStorage' in window && window['localStorage'] !== null;
	} catch (e) {
		return false;
	}
}

/*
 * Name: checkInput
 * Description: checks the input of a given field to make sure that it conforms to parameters
 * Argument: field = field to check input from
 * Returns: nothing;
 */
function checkInput(field) {
	if ( typeof field == "undefined") {
		return false;
	}
	var field_id = "#" + field;
	var field_error = field_id + "_error";
	var field_data = $(field_id).val();
	var valid = true;

	/* don't fire if field is empty */
	if (field_data == "") {
		clearErrors();
		return false;
	} else {
		field_data = parseInt(field_data);
	}

	/* check for correct size */
	switch (field) {
	case "diceascii":
	case "dicechar":
		if ((field_data < 111) || (field_data > 666)) {
			valid = false;
		}
		break;
	case "dicedecnum":
	case "dicehexnum":
	case "dicewordchar":
		if ((field_data < 11) || (field_data > 66)) {
			valid = false;
		}
		break;
	case "diceword":
		if ((field_data < 11111) || (field_data > 66666)) {
			valid = false;
		}

		break;
	case "dicewordcaps":
		if ((field_data < 1) || (field_data > 6)) {
			valid = false;
		}
		break;
	}

	/* check if in range */
	if (valid) {
		valid = inRange(field_data);
	}

	/* display message, if necessary */
	if (!valid) {
		$(field_error).addClass("erroractive");
		errorState = true;
	} else {
		errorState = false;
		clearErrors();
	}
}

/*
 * Name: checkTabs
 * Description: checks the checkboxes on the preferences page
 *              and makes sure that one remains checked
 * Arguments: none
 * Returns: nothing
 */
function checkTabs() {
	var tablist = ["prefdw","prefda","prefdc","prefdd","prefdh"];
	var checked = 0;
	for (var i=0; i<tablist.length; i++) {
		if ($("#"+tablist[i]).is(":checked")) {
			checked++;
		}
		if ($("#"+tablist[i]).is(":disabled")) {
			$("#"+tablist[i]).attr("disabled",false).checkboxradio("refresh");
		}
	}
	if (checked < 2) {
		for (var i=0; i<tablist.length; i++) {
		if ($("#"+tablist[i]).is(":checked")) {
			$("#"+tablist[i]).attr("disabled",true).checkboxradio("refresh");
		}
		}
	}
	setTabSelect(false);
}


/*
 * Name: clearErrors
 * Description: clears all error notices
 * Arguments: none
 * Returns: nothing
 */
function clearErrors() {
	var errorlist = ["diceword", "dicewordchar", "dicewordcaps", "diceascii", "dicechar", "dicedecnum", "dicehexnum"];
	for (var i = 0; i < errorlist.length; i++) {
		if ($("#" + errorlist[i] + "_error").hasClass("erroractive")) {
			$("#" + errorlist[i] + "_error").removeClass("erroractive");
		}
	}
}

/*
 * Name: clearPassPhrase
 * Description: clears the passphrase field
 * Argument: field = field to check input from
 * Returns: nothing;
 */
function clearPassPhrase() {
	$("#result").html("");
	passphrase = [];
	phraseLength();
}

/*
 * Name: copyPassPhrase
 * Description: copies stuff to clipboard using HTML5 wrapper
 * Argument: none
 * Returns: nothing
 */
function copyPassPhrase() {
	$("html, body").animate({
		scrollTop : 0
	});
	$("#copytext").addClass("erroractive");
	$("#result").focus();
}

/*
 * Name: delLastEntry
 * Description: clears the last piece of the passphrase
 * Arguments: none
 * Returns: nothing;
 */
function delLastEntry() {
	if (passphrase.length > 0) {
		passphrase.pop();
	}
	if (passphrase.length > 0) {
		writePassphrase();
	} else {
		clearPassPhrase();
	}
}

/*
 * Name: focusRollBox
 * Description: sets focus on the dice* roll box
 * Arguments: none
 * Returns: nothing
 */
function focusRollBox() {
		/* focus on the main input field */
	switch ($("#tablist").val()) {
		case "phrase":
			boxname = "word";
			break;
		case "ascii":
			boxname = "ascii";
			break;
		case "letnum":
			boxname = "char";
			break;
		case "dec":
			boxname = "decnum";
			break;
		case "hex":
			boxname = "hexnum";
			break;
	}
	$("#dice" + boxname).focus();

}

/*
 * Name: genPassPhrase
 * Description: generates a passphrase using the passed field
 * Argument: pptype = Which type of passphrase to generate
 * Returns: nothing
 */
function genPassPhrase(pptype) {
	if ( typeof pptype == "undefined") {
		return false;
	}
	if (errorState) {
		return false;
	}

	clearErrors();

	var pp = "";
	var pptwo = "";
	var dice = "";
	var capchar = -1;

	/* check to see if we're dealing with a new passphrase */
	if (passPhraseType != pptype) {
		clearPassPhrase();
		passPhraseType = pptype;
	}

	switch (pptype) {
	case "ascii":
		dice = $("#diceascii").val();
		if (inRange(dice)) {
			pp = genAsciiCode(dice);
		} else {
			return false;
		}
		$("#diceascii").val("");
		break;
	case "azdec":
		dice = $("#dicechar").val();
		if (inRange(dice)) {
			pp = genAlphaNumeric(dice);
		} else {
			return false;
		}
		$("#dicechar").val("");
		break;
	case "dec":
		dice = $("#dicedecnum").val();
		if (inRange(dice)) {
			pp = genRandomNumber(dice);
		} else {
			return false;
		}
		if (pp == "*") {
			alert("Please roll the first die again!");
			$("#dicedecnum").focus();
			return false;
		} else {
			$("#dicedecnum").val("");
		}
		break;
	case "hex":
		dice = $("#dicehexnum").val();
		if (inRange(dice)) {
			pp = genHexNumber(dice);
		} else {
			return false;
		}
		$("#dicehexnum").val("");
		break;
	case "word":
		/* get word */
		dice = $("#diceword").val();
		if (inRange(dice)) {
			pp = findVal(dice);
		} else {
			return false;
		}
		/* check for capitalization */
		if ($("#diceword_caps").is(":checked")) {
			dice = $("#dicewordcaps").val();
			if (inRange(dice)) {
				while (dice > pp.length) {
					dice = dice - pp.length;
				}
				capchar = dice-1;
				pptwo = pp;
				pp =  pptwo.substr(0, capchar) + pptwo.substr(capchar, 1).toUpperCase();
				if (capchar < (pptwo.length - 1)) {
					pp = pp + pptwo.substr((capchar + 1));
				}
			}
		}
		/* check for extra chars */
		if ($("#spacer_char").is(":checked")) {
			dice = $("#dicewordchar").val();
			if (inRange(dice)) {
				pp = pp + findVal(dice);
			}
		} else if ($("#spacer_dec").is(":checked")) {
			dice = $("#dicewordchar").val();
			if (inRange(dice)) {
				pp = pp + genRandomNumber(dice);
			}
		} else if ($("#spacer_space").is(":checked")) {
			pp = pp + " ";
		}
		/* reset the items */
		$("#dicewordcaps").val("");
		$("#dicewordchar").val("");
		$("#diceword").val("");
		if ($("#diceword_caps").is(":checked")) {
			$("#diceword_caps").attr("checked", false).checkboxradio("refresh");
			toggleCaps();
		}
		break;
	}
	passphrase[passphrase.length] = pp;
	writePassphrase();
	$("html, body").animate({
		scrollTop : 0
	});
}

/*
 * Name: hideCopyText
 * Description: hides the copy text
 * Arguments: none
 * Returns: nothing
 */
function hideCopyText() {
	if ($("#copytext").hasClass("erroractive")) {
		$("#copytext").removeClass("erroractive");
	}
}

/*
 * Name: inRange
 * Description: checks to see if a passed item is within a given range
 * Argument: item = dice roll number to check
 * Returns: boolean denoting range
 */
function inRange(item) {
	var itemlength = item.toString().length;
	var digits = new Array();
	var maxdigit = "";
	var enddigit = 0;
	var digit = 0;
	var nextlength = 0;
	var incnext = false;

	/* create maximum digits */
	for (var i = 1; i < itemlength + 1; i++) {
		digits[i] = 1;
		maxdigit = maxdigit + "6";
	}
	enddigit = parseInt(maxdigit) + 1;
	/* begin building items to check against range */
	while (digit != maxdigit && (digit < enddigit)) {
		digit = digits.join("");
		if (item == digit) {
			return true;
		}
		digits[itemlength]++;
		if (digits[itemlength] > 6) {
			digits[itemlength] = 1;
			incnext = true;
			nextlength = itemlength - 1;
		}
		while (incnext && (nextlength > 0)) {
			digits[nextlength]++;
			if ((digits[nextlength] > 6) && (nextlength > 0)) {
				digits[nextlength] = 1;
				nextlength--;
			} else {
				incnext = false;
			}
		}
	}
	return false;
}

/*
 * Name: loadLang
 * Description: loads the language strings and populates them
 * Arguments: usedefault = boolean whether or not to use default language (en-us)
 * Returns: nothing
 */
function loadLang(usedefault) {
	if (typeof usedefault == "undefined") { usedefault = false; }
	var langid = navigator.language.toLowerCase();
	switch (langid) {
		default:
			lang = localization.en_us;
			break;
	}
	$("#loaddescriptor").html(lang.loaddescriptor);
	$("#loadstatus").html(lang.loadlocalizing);
	$("#description").html(lang.description);
	$("#copytext").html(lang.copytext);
	$("#result").attr("placeholder",lang.result_placeholder);
	$("#charcounttxt").html(lang.charcounttxt);
	$("#copybutton").html(lang.copybutton);
	$("#dellastbutton").html(lang.dellastbutton);
	$("#clearbutton").html(lang.clearbutton);
	$("#prefsbutton").html(lang.prefsbutton);
	$("#prefstitle").html(lang.prefstitle);
	$("#prefstabs").html(lang.prefstabs);
	$("#prefstabsblurb").html(lang.prefstabsblurb);
	$("#prefdwtext").html(lang.prefsdwtext);
	$("#prefdatext").html(lang.prefdatext);
	$("#prefdctext").html(lang.prefdctext);
	$("#prefddtext").html(lang.prefddtext);
	$("#prefdhtext").html(lang.prefdhtext);
	$("#prefsdeftabtitle").html(lang.prefsdeftabtitle);
	$("#prefsdeftabdw").html(lang.prefsdeftabdw);
	$("#prefsdeftabda").html(lang.prefsdeftabda);
	$("#prefsdeftabdc").html(lang.prefsdeftabdc);
	$("#prefsdeftabdd").html(lang.prefsdeftabdd);
	$("#prefsdeftabdh").html(lang.prefsdeftabdh);
	$("#prefsdwspacenone").html(lang.prefsdwspacenone);
	$("#prefsdwspacespace").html(lang.prefsdwspacespace);
	$("#prefsdwspacechar").html(lang.prefsdwspacechar);
	$("#prefsdwspacerdec").html(lang.prefsdwspacerdec);
	$("#prefsdwcaps").html(lang.prefsdwcaps);
	$("#prefsdwcapstext").html(lang.prefsdwcapstext);
	$("#prefssave").html(lang.prefssave);
	$("#prefscancel").html(lang.prefscancel);
	$("#prefsreset").html(lang.prefsreset);
	$("#abouta").html(lang.aboutbutton);
	$("#abouttitle").html(lang.abouttitle);
	$("#aboutblurb").html(lang.aboutblurb);
	$("#abouthowtitle").html(lang.abouthowtitle);
	$("#abouthowtext").html(lang.abouthowtext);
}

/*
 * Name: loadPrefs
 * Description: loads the preferences, first from localStorage (if exits), then from defaults
 * Arguments: init = whether we're initializing or not
 * Returns: nothing
 */
function loadPrefs(init) {
	if (typeof init == "undefined") { init = true; }
	var local = checkForLocalStorage();
	/* read local preferences */
	if (local && localStorage.getItem("init_tab") != null) {
		prefs = {
			"show_phrase" : localStorage.getItem("show_phrase"),
			"show_ascii" : localStorage.getItem("show_ascii"),
			"show_alphanum" : localStorage.getItem("show_alphanum"),
			"show_decimal" : localStorage.getItem("show_decimal"),
			"show_hex" : localStorage.getItem("show_hex"),
			"init_tab" : localStorage.getItem("init_tab"),
			"phrase_spacer" : localStorage.getItem("phrase_spacer"),
			"phrase_caps" : localStorage.getItem("phrase_caps")
		};
	} else {
		prefs = defaultPrefs;
	}

	/* write out local preferences */
	if (local) {
		$.each(prefs, function(key, value) {
			localStorage.setItem(key, value);
		});
	}

	/* update preferences page */
	/* tabs to show */
	if (prefs.show_phrase == "true") {
		$("#prefdw").prop("checked", true);
	} else {
		$("#prefdw").removeAttr("checked");
	}
	if (!init) { $("#prefdw").checkboxradio("refresh");}
	if (prefs.show_ascii == "true") {
		$("#prefda").prop("checked", true);
	} else {
		$("#prefda").removeAttr("checked");
	}
	if (!init) { $("#prefdd").checkboxradio("refresh");}
	if (prefs.show_alphanum == "true") {
		$("#prefdc").prop("checked", true);
	} else {
		$("#prefdc").removeAttr("checked");
	}
	if (!init) { $("#prefdc").checkboxradio("refresh");}
	if (prefs.show_decimal == "true") {
		$("#prefdd").prop("checked", true);
	} else {
		$("#prefdd").removeAttr("checked");
	}
	if (!init) { $("#prefdd").checkboxradio("refresh");}
	if (prefs.show_hex == "true") {
		$("#prefdh").prop("checked", true);
	} else {
		$("#prefdh").removeAttr("checked");
	}
	if (!init) { $("#prefdh").checkboxradio("refresh");}

	/* set individual preferences */
	setTabSelect(true);
	switch(prefs.init_tab) {
	case "phrase":
		$("#prefsdeftabdw").attr("selected", true);
		break;
	case "ascii":
		$("#prefsdeftabda").attr("selected", true);
		break;
	case "lettnum":
		$("#prefsdeftabdc").attr("selected", true);
		break;
	case "decimals":
		$("#prefsdeftabdd").attr("selected", true);
		break;
	case "hexadecimals":
		$("#prefsdeftabdh").attr("selected", true);
		break;
	}
	if (!init) { $("prefsdeftab").selectmenu("refresh");}
	/* set Passphrase preferences */
	$("#prefsdwspace" + prefs.phrase_spacer).attr("selected", true);
	if (prefs.phrase_caps == "true") {
		$("#prefdwcapsbox").prop("checked", "checked");
	} else {
		$("#prefdwcapsbox").removeAttr("checked");
	}
	if (!init) { $("#prefdwcapsbox").checkboxradio("refresh");}

	/* disable passphrase preferences, if necessary */
	if (prefs.show_phrase == true) {
		$("#prefsdwspacersel").attr("disabled", true);
		$("#prefdwcapsbox").attr("disabled", true);
	} else {
		$("#prefsdwspacersel").removeAttr("disabled");
		$("#prefdwcapsbox").removeAttr("disabled");
	}
	if (!init) { $("#prefsdwspacersel").selectmenu("refresh");}
	if (!init) { $("#prefdwcapsbox").checkboxradio("refresh");}

	/* window.location.hash = "#mainpage"; */
}

/*
 * Name: phraseLength
 * Description: calculates how many characters are in the current passphrase
 * Arguments: none
 * Returns:nothing
 */
function phraseLength() {
	var i;
	var pl = 0;
	if (passphrase.length > 0){
		for (i=0; i<passphrase.length; i++) {
			pl = pl + passphrase[i].length;
		}
	}
	$("#charcountnum").html(pl);
}

/*
 * Name: resetDefaultPrefs
 * Description: resets all preferences to defaults and reloads theme
 * Arguments: none
 * Returns: nothing
 */
function resetDefaultPrefs() {
	var local = checkForLocalStorage();

	/* load defaults */
	prefs = defaultPrefs;

	/* write out local preferences */
	if (local) {
		$.each(prefs, function(key, value) {
			localStorage.setItem(key, value);
		});
	}
	loadPrefs(false);
	applyPrefs(false);
	/* load main page; preferences are applied upon loading */
	window.location.hash = "#mainpage";
}

/*
 * Name: savePrefs
 * Description: saves the preferences to local storage and updates them
 * Arguments: none
 * Returns: nothing
 */
function savePrefs() {
	var local = checkForLocalStorage();

	/* update prefs object */
	if ($("#prefdw").is(":checked")) {
		prefs.show_phrase = "true";
	} else {
		prefs.show_phrase = "false";
	}
	if ($("#prefda").is(":checked")) {
		prefs.show_ascii = "true";
	} else {
		prefs.show_ascii = "false";
	}
	if ($("#prefdc").is(":checked")) {
		prefs.show_alphanum = "true";
	} else {
		prefs.show_alphanum = "false";
	}
	if ($("#prefdd").is(":checked")) {
		prefs.show_decimal = "true";
	} else {
		prefs.show_decimal = "false";
	}
	if ($("#prefdh").is(":checked")) {
		prefs.show_hex = "true";
	} else {
		prefs.show_hex = "false";
	}
	prefs.init_tab = $("#prefsdeftab").val();
	prefs.phrase_spacer = $("#prefsdwspacersel").val();
	if ($("#prefdwcapsbox").is(":checked")) {
		prefs.phrase_caps = "true";
	} else {
		prefs.phrase_caps = "false";
	}

	/* write out local preferences */
	if (local) {
		$.each(prefs, function(key, value) {
			localStorage.setItem(key, value);
		});
	}

	applyPrefs(false);
	/* load main page; preferences are applied upon loading */
	window.location.hash = "#mainpage";
}

/*
 * Name: setTabSelect
 * Description: sets the available select options on the tabs
 * Arguments: init = whether or not we're initializing the list
 * Returns: nothing
 */
function setTabSelect(init) {
	if (typeof init == "undefined") { init = false; }
	var tablist = ["dw","da","dc","dd","dh"];
	var tabnames= [lang.prefsdeftabdw, lang.prefsdeftabda, lang.prefsdeftabdc, lang.prefsdeftabdd, lang.prefsdeftabdh];
	var tabvalue = ["phrase","ascii","lettnum","desc","hex"];
	var prefix = "prefsdeftab";
	var html = "";

	/* empty select */
	$("#"+prefix).html("");

	/* add back options */
	for (var i=0;i<tablist.length; i++) {
		if ($("#pref"+tablist[i]).is(":checked")) {
			html = "<option id=\"" + prefix + tablist[i] + "\" name=\"" + prefix + tablist[i] + "\"";
			if (i == 0) { html += " selected"; }
			html += " value =\"" + tabvalue[i] + "\">" + tabnames[i] + "</option>";
			$("#"+prefix).append(html);
		}
	}
	if (!init) { $("#"+prefix).selectmenu("refresh"); }
}


/*
 * Name: showTab
 * Description: shows the selected tab
 * Arguments: none
 * Returns: nothing
 */
function showTab() {
	var boxname = "";
	/* iterate through options and hide the visible ones */
	$("#tablist > option").each(function() {
		if ($("#"+this.value).css("display") != "none") {
			$("#"+this.value).toggle();
		}
	});

	/* display the selected option */
	$("#" + $("#tablist").val()).toggle();

	focusRollBox();
}
/*
 * Name: tabContent
 * Description: returns the html for a given tab
 * Arguments: id = identifier of the tab to return:
 * 				   "phrase","ascii","lettnum","dec","hex"
 * Returns: a string containing generated HTML
 */
function tabContent(id) {
	if (typeof id == "undefined") { return false; }
	var html = "";
	switch (id) {
		case "phrase":
			html = "<h3>" + lang.dwhead + "</h3>\n";
			html += "<p><label>" + lang.dwword + ": <input type=\"number\" size=\"7\" min=\"11111\" max=\"66666\" name=\"diceword\" id=\"diceword\" onblur=\"checkInput('diceword')\"></label></p>\n";
			html += "<p id=\"diceword_error\" class=\"errorbox\">" + lang.diceword_error + "</p>\n";
			html += "<p><label><input type=\"radio\" name=\"spacers\" id=\"spacer_none\" onclick=\"toggleChars('hide');\"> " + lang.dwnosep + "</label>";
			html += "<label><input type=\"radio\" name=\"spacers\" id=\"spacer_space\" onclick=\"toggleChars('hide');\"> " + lang.dwspaces + "</label>";
			html += "<label><input type=\"radio\" name=\"spacers\" id=\"spacer_char\" onclick=\"toggleChars('show');\"> " + lang.dwchars + "</label>";
			html += "<label><input type=\"radio\" name=\"spacers\" id=\"spacer_dec\" onclick=\"toggleChars('show');\"> " + lang.dwdec + "</label></p>\n";
			html += "<p id=\"specchars\" style=\"display: none;\"><label><span id=\"dwchartitle\">" + lang.dwcharr + "</span>: <input type=\"number\" size=\"4\" min=\"11\" max=\"66\" name=\"dicewordchar\" id=\"dicewordchar\" onblur=\"checkInput('dicewordchar')\"></label></p>\n";
			html += "<p id=\"dicewordchar_error\" class=\"errorbox\">" + lang.dicewordchar_error + "</p>\n";
			html += "<p><label><input type=\"checkbox\" id=\"diceword_caps\" onclick=\"toggleCaps();\"> " + lang.dwcapscheck + "</label></p>\n";
			html += "<p id=\"addcaps\" style=\"display: none;\"><label>" + lang.dwcaps + ": <input type=\"number\" size=\"2\" min=\"1\" max=\"6\" name=\"dicewordcaps\" id=\"dicewordcaps\" onblur=\"checkInput('dicewordcaps')\"></label></p>\n";
			html += "<p id=\"dicewordcaps_error\" class=\"errorbox\">" + lang.dicewordcaps_error + "</p>\n";
			html += "<p><button onclick=\"genPassPhrase('word');\" data-icon=\"plus\" id=\"dwgenbutton\">" + lang.dwgenbutton + "</button></p>";
			break;
		case "ascii":
			html = "<h3>" + lang.datitle + "</h3>\n";
			html += "<p><label>" + lang.daroll + ": <input type=\"number\" size=\"4\" min=\"111\" max=\"666\" name=\"diceascii\" id=\"diceascii\" onblur=\"checkInput('diceascii')\"></label></p>\n";
			html += "<p id=\"diceascii_error\" class=\"errorbox\">" + lang.diceascii_error + "</p>\n";
			html += "<p><button onclick=\"genPassPhrase('ascii');\" data-icon=\"plus\" id=\"dabutton\">" + lang.dabutton + "</button></p>";
			break;
		case "lettnum":
			html = "<h3>" + lang.dctitle + "</h3>\n";
			html += "<p><label>" + lang.dcroll + ": <input type=\"number\" size=\"4\" min=\"111\" max=\"666\" name=\"dicechar\" id=\"dicechar\" onblur=\"checkInput('dicechar')\"></label></p>\n";
			html += "<p id=\"dicechar_error\" class=\"errorbox\">" + lang.dicechar_error + "</p>\n";
			html += "<p class=\"note\">" + lang.dcnote + "</p>\n";
			html += "<p><button onclick=\"genPassPhrase('azdec');\" data-icon=\"plus\" id=\"dcbutton\">" + lang.dcbutton + "</button></p>";
			break;
		case "dec":
			html = "<h3>" + lang.ddtitle + "</h3>\n";
			html += "<p><label>" + lang.ddroll + ": <input type=\"number\" size=\"3\" min=\"11\" max=\"66\" name=\"dicedecnum\" id=\"dicedecnum\" onblur=\"checkInput('dicedecnum')\"></label></p>\n";
			html += "<p id=\"dicedecnum_error\" class=\"errorbox\">" + lang.dicedecnum_error + "</p>\n";
			html += "<p class=\"note\">" + lang.ddnote + "</p>";
			html += "<p><button onclick=\"genPassPhrase('dec');\" data-icon=\"plus\" id=\"ddbutton\">" + lang.ddbutton + "</button></p>\n";
			break;
		case "hex":
			html = "<h3>" + lang.dhtitle + "</h3>\n";
			html += "<p><label>" + lang.dhroll + ": <input type=\"number\" size=\"3\" min=\"11\" max=\"66\" name=\"dicehexnum\" id=\"dicehexnum\" onblur=\"checkInput('dicehexnum')\"></label></p>\n";
			html += "<p id=\"dicehexnum_error\" class=\"errorbox\">" + lang.dicehexnum_error + "</p>\n";
			html += "<p><button onclick=\"genPassPhrase('hex');\" data-icon=\"plus\" id=\"dhbutton\">" + lang.dhbutton + "</button></p>";
			break;
	}
	return html;
}

/*
 * Name: toggleCaps
 * Description: shows or hides the capitalize characters roll field;
 * Argument: none
 * Returns: nothing
 */
function toggleCaps() {
	$("#addcaps").slideToggle();
	if ($("#addcaps").css("display") != "none") {
		$("#dicewordcaps").focus();
	}
}

/*
 * Name: toggleChars
 * Description: shows or hides the special characters roll field;
 * Argument: action = 'show' or 'hide'
 * Returns: nothing
 */
function toggleChars(action) {
	if ( typeof action == "undefined") {
		return false;
	}
	switch (action) {
	case "hide":
		if ($("#specchars").css("display") != "none") {
			$("#specchars").slideToggle();
		}
		break;
	case "show":
		if ($("#specchars").css("display") == "none") {
			$("#specchars").slideToggle();
		}
		if ($("#spacer_char").is(":checked")) {
			$("#dwchartitle").html(lang.dwcharr);
		} else if ($("#spacer_dec").is(":checked")) {
			$("#dwchartitle").html(lang.dwdecr);
		}
		$("#dicewordchar").focus();
		break;
	}
}

/*
 * Name: writePassphrase()
 * Description: writes the passphrase to the result
 * Arguments: none
 * Returns: nothing
 */
function writePassphrase() {
	$("#result").html(passphrase.join(""));
	$("#copyfield").val(passphrase.join(""));
	phraseLength();
}

/* jQuery functions */
/* initialize stuff when document loads */
$(function() {
	/* localize interface */
	$("#loadstatus").html("Localizing Interface...");
	loadLang();

	/* load preferences */
	$("#loadstatus").html(lang.loadpreferences);
	loadPrefs();

	/* Build tabs */
	$("#loadstatus").html(lang.loadinterface);
	applyPrefs(true);

	/* finish */
	$("#loadstatus").html(lang.loadcomplete);
	initializing = false;
	window.location.hash = "#mainpage";

});
