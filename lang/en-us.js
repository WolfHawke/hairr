/*
 * Hawke AI Random Roller
 * Language file
 *
 * Language: en-us
 *
 */

lang = {
	"loaddescriptor" : "A Diceware-based truly random password generator",
	"description":"Generate truly random passwords by rolling real dice and entering the sequence in the box below.",
	"copytext":"Now press Ctrl+C to copy the passphrase.",
	"result_placeholder":"This is where your generated content will appear.",
	"dicewords":"Phrase",
	"diceasciitab":"aB7#",
	"dicealphanum":"Ab9",
	"dicenum":"0-9",
	"dicehex":"0-F",
	"dwhead":"Passphrase (Multiple Words and Optional Special Characters)",
	"dwword":"Diceware word (roll 5 dice)",
	"diceword_error":"The number you have entered is not a valid 5-dice roll. Please check your number and try again.",
	"dwnosep":"No separation between words",
	"dwspaces":"Use Spaces between words",
	"dwchars": "Use numbers or special characters between words",
	"dwcharr":"Diceware character (roll 2 dice)",
	"dicewordchar_error":"The number you have entered is not a valid 2-dice roll. Please check your number and try again.",
	"dwcapscheck":"Add caps to passphrase",
	"dwcaps":"Diceware letter to capitaize (roll 1 die)",
	"dicewordcaps_error":"The number you have entered is not a valid 1-die roll. Please check your number and try again.",
	"dwgenbutton":"Add Word to Passphrase",
	"datitle":"Password (Letters, Numbers and special Characters)",
	"daroll":"Diceware character (roll 3 dice)",
	"diceascii_error":"The number you haerve entered is not a valid 3-dice roll. Please check your number and try again.",
	"dabutton":"Add Character to Password",
	"dctitle":"Password (Letters and Numbers)",
	"dcroll":"Diceware character (roll 3 dice)",
	"dicechar_error":"The number you have entered is not a valid 3-dice roll. Please check your number and try again.",
	"dcnote":"To generate all uppercase letters, roll 2 dice and make the third digit a 2. For all lowercase letters the third digit should be 1.",
	"dcbutton":"Add Character to Password",
	"ddtitle":"Random Decimal Numbers",
	"ddroll":"Diceware decimal number (roll 2 dice)",
	"dicedecnum_error":"The number you have entered is not a valid 2-dice roll. Please check your number and try again.",
	"ddnote":"If you roll a 6 with the first die, roll that die again.",
	"ddbutton":"Add Digit to Number",
	"dhtitle":"Random Hexadecimal Numbers",
	"dhroll": "Diceware hexadecimal number (roll 2 dice)",
	"diceexnum_error":"The number you have entered is not a valid 2-dice roll. Please check your number and try again.",
	"dhbutton":"Add Digit to Number",
	"copybutton":"Copy Passphrase to Clipboard",
	"dellastbutton": "Remove Last Item",
	"clearbutton":"Clear Passphrase",
	"prefsbutton": "Preferences"

};
